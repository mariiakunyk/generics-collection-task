package com.epam.javalab.StringArray;

public class Container {
    private String[] array = new String[10];
    private static int position = 0;

    public void add(String s) {
        String[] newArray = new String[2 * array.length];
        if (array.length <= position) {
            System.arraycopy(array, 0, newArray, 0, array.length);
            newArray[position] = s;
            position++;
            this.array = newArray;
        } else {
            array[position] = s;
            position++;
        }
    }


    private String getString(int index) {
        if (array.length < index) {
            return "no such element";
        }
        String ss = array[index];
        array[index] = null;
        for (int i = index; i < array.length - 1; i++) {
            array[i] = array[i + 1];
            array[i + 1] = null;
        }
        return ss;
    }

    private void print() {
        for (int i = 0; i < array.length; i++) {
            if(array[i] != null) {
                System.out.println(array[i]);
            }
        }
    }

    public static void main(String[] args) {
        Container container = new Container();
        container.add("animals");
        container.add("cat");

        container.add("dog");
        container.add("giraffe");
        container.add("bird");
        container.add("butterfly");
        container.add("frog");
        container.add("elephant");
        container.add("crocodile");
        container.add("list");
        container.add("map");
        container.add("queue");
        container.add("dequeue");
        container.add("arraylist");
        System.out.println("My string container contains: ");
        container.print();
        System.out.println("\n\n\n");
        System.out.print("Get first element: ");
        System.out.println(container.getString(0));
        System.out.print("Get first element: ");
        System.out.println(container.getString(0));
        System.out.print("Get third element: ");
        System.out.println(container.getString(2));
        System.out.println("\n\n\n");
        container.print();
    }
}
