package com.epam.javalab.StringPair;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BoxForString implements Comparable<BoxForString> {
    private String nameOfCountry;
    private String nameOfCapital;

    public String getNameOfCountry() {
        return nameOfCountry;
    }

    public void setNameOfCountry(String nameOfCountry) {
        this.nameOfCountry = nameOfCountry;
    }

    public String getNameOfCapital() {
        return nameOfCapital;
    }

    public void setNameOfCapital(String nameOfCapital) {
        this.nameOfCapital = nameOfCapital;
    }

//    @Override
//    public int compareTo(com.epam.javalab.StringPair.BoxForString o) {
//        return this.nameOfCountry.length() - o.nameOfCountry.length();
//    }

    @Override
    public int compareTo(BoxForString o) {
        return this.nameOfCapital.compareTo(o.nameOfCapital);
    }

    public List<BoxForString> generateArrayListWithString() {
        List<BoxForString> boxForStrings = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("src/main/resources/country-capital.txt"))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                BoxForString boxForString = new BoxForString();
                String[] info = sCurrentLine.split(" ");
                boxForString.setNameOfCountry(info[0]);
                boxForString.setNameOfCapital(info[1]);
                boxForStrings.add(boxForString);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return boxForStrings;
    }

//    public com.epam.javalab.StringPair.BoxForString[] generateArrayWithString() {
//        com.epam.javalab.StringPair.BoxForString[] boxForStrings = new com.epam.javalab.StringPair.BoxForString[6];
//        try (BufferedReader br = new BufferedReader(new FileReader("src/main/resources/country-capital.txt"))) {
//            String sCurrentLine;
//            while ((sCurrentLine = br.readLine()) != null) {
//                for (int i = 0; i < boxForStrings.length; i++) {
//                    com.epam.javalab.StringPair.BoxForString boxForString = new com.epam.javalab.StringPair.BoxForString();
//                    String[] info = sCurrentLine.split(" ");
//                    boxForString.setNameOfCountry(info[0]);
//                    boxForString.setNameOfCapital(info[1]);
//                    boxForStrings[i] = boxForString;
//                }
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return boxForStrings;
//    }


    public static void main(String[] args) {
        BoxForString boxForString = new BoxForString();
        // com.epam.javalab.StringPair.BoxForString[] boxForStrings = boxForString.generateArrayWithString();
        List<BoxForString> boxForStrings = boxForString.generateArrayListWithString();
        for (int i = 0; i < boxForStrings.size() - 1; i++) {
            Collections.sort(boxForStrings);
        }
        for (int i = 0; i < boxForStrings.size(); i++) {
            System.out.println(boxForStrings.get(i).getNameOfCountry());
            //System.out.println(boxForStrings.get(i).getNameOfCapital());
        }

    }
}
