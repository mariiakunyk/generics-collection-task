package com.epam.javalab.Deque;

public class Deque<T> {
    private Node<T> head;
    private Node<T> tail;

    private static class Node<T> {
        T item;
        Node<T> next;
        Node<T> previous;

        public Node(T item, Node<T> next, Node<T> previous) {
            this.item = item;
            this.next = next;
            this.previous = previous;
        }

    }


    public void addFirst(T element) {
        if (head == null) {
            head = new Node<>(element, null, null);
        } else {
            head.previous = new Node<>(element, head, null);
            head = head.previous;
        }
    }

    public void addLast(T element) {
        Node<T> temp = head;
        if (head == null) {
            head = new Node<>(element, null, null);
        } else {
            while (temp.next != null) {
                temp = temp.next;
            }
            Node<T> newNode = new Node<>(element, null, temp);
            temp.next = newNode;
        }
    }

    public T removeFirst() {
        if (head == null) {
            return null;
        }
        T element = head.item;
        head = head.next;
        return element;
    }

    // retrieve and remove element at the head
    public T remove() {
        if (head == null) {
            return null;
        }
        T element = head.item;
        head = head.next;
        return element;
    }

    public T removeLast() {
        if (head == null) {
            return null;
        }
        Node<T> temp = head;
        while (temp.next != null) {
            temp = temp.next;
        }
        T element = temp.item;
        temp = temp.previous;
        temp.next = null;
        return element;
    }

    public void print() {
        Node<T> temp = head;
        while (temp != null) {
            System.out.print(temp.item + " ");
            temp = temp.next;
        }
        System.out.println();
    }

    public void size() {
        Node<T> temp = head;
        while (temp != null) {
            System.out.println(temp.item);
        }
    }

    public T poll() {
        if (head == null) {
            return null;
        } else if (head.next == null) {
            T element = head.item;
            head = null;
            return element;
        } else {
            T element = head.item;
            head = head.next;
            return element;
        }
    }

    public void add(T element) {
        addLast(element);
    }

    public boolean contains(T element) {
        Node<T> temp = head;
        if (head == null) {
            return false;
        }
        while (temp.item != element) {
            temp = temp.next;
            if (temp == null) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Deque<Integer> integerDeque = new Deque<>();
        integerDeque.addFirst(2);
        integerDeque.addFirst(1);
        integerDeque.addFirst(3);
        integerDeque.addLast(1);
        integerDeque.addLast(6);
        System.out.println("Start queue");
        integerDeque.print();
        System.out.println("\n\n");
        System.out.print("Remove last element:");
        System.out.println(integerDeque.removeLast());
        integerDeque.print();
        System.out.print("Remove first element: ");
        System.out.println(integerDeque.removeFirst());
        integerDeque.print();
        System.out.print("\n\nPoll element: ");
        System.out.println(integerDeque.poll());
        integerDeque.print();
        System.out.println("\n");
        System.out.println("add elements 5, 7 and 9");
        integerDeque.add(5);
        integerDeque.add(7);
        integerDeque.add(9);
        integerDeque.print();
        integerDeque.remove();
        System.out.println("Remove element");
        integerDeque.print();
    }
}