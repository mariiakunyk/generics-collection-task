package com.epam.javalab.droidship;

import java.util.Comparator;

public class Droid implements Comparable<Droid> {
    String name;
    String weapon;
    int quantityOfWeapon;

    public int getQuantityOfWeapon() {
        return quantityOfWeapon;
    }

    public Droid(String name, String weapon, int quantityOfWeapon) {
        this.name = name;
        this.weapon = weapon;
        this.quantityOfWeapon = quantityOfWeapon;
    }

    public Droid() {

    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Droid droid) {
        return this.quantityOfWeapon - droid.getQuantityOfWeapon();
    }

    public static class SortByQuantity implements Comparator<Droid> {

        @Override
        public int compare(Droid o1, Droid o2) {
            return o1.quantityOfWeapon - o2.quantityOfWeapon;
        }
    }

//    public static Comparator<Droid> QuantityOfWeaponComparator = new Comparator<Droid>() {
//        @Override
//        public int compare(Droid o1, Droid o2) {
//            int quantityFirst = o1.quantityOfWeapon;
//            int quantityTwo = o2.quantityOfWeapon;
//            return o1.compareTo(o2);
//        }
//    };


    @Override
    public String toString() {
        return "" + quantityOfWeapon;
    }
}
