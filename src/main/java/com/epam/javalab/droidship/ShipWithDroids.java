package com.epam.javalab.droidship;

import java.util.*;

public class ShipWithDroids<T extends Droid> {
    private List<T>  listOfDroids;
    public ShipWithDroids(List<T> droid) {
        this.listOfDroids = droid;
    }

    public List<T> getListOfDroids() {
        return listOfDroids;
    }

    public void addDroid(T droid){
        listOfDroids.add(droid);
    }

    public T getDroidFromShip(int index){
        if (listOfDroids.size() < index - 1){
            throw new NoSuchElementException();
        }
        T droid = listOfDroids.get(index - 1);
        listOfDroids.remove(index - 1);
        return droid;
    }

    void print(){
        for(int i = 0; i < listOfDroids.size(); i++){
            System.out.println(listOfDroids.get(i).getName() + " " + listOfDroids.get(i).getQuantityOfWeapon());
        }
    }

    public static void main(String[] args) {
        List<Droid> list = new ArrayList<>();
        list.add(new AnotherDroid("olya","colt", 1,2));
        list.add(new RepairDroid("misha","colt", 3,0.5));
        list.add(new AnotherDroid("kolya","colt",2, 1));
        ShipWithDroids<Droid> droidShipWithDroids = new ShipWithDroids<>(list);
        droidShipWithDroids.addDroid(new AnotherDroid("df", "colt", 4,5));
        droidShipWithDroids.print();
        System.out.println("\n\n");
        droidShipWithDroids.getDroidFromShip(2);
        System.out.println("Get com.epam.javalab.droid from ship with index 2");
        droidShipWithDroids.print();
        Collections.sort(droidShipWithDroids.getListOfDroids(), Droid::compareTo);
        System.out.println("Sorted droids");
        droidShipWithDroids.print();
    }
}
