package com.epam.javalab.droidship;

public class RepairDroid extends Droid {

    private double probabilityToRepair;

    public RepairDroid(String name, String weapon, int quantity, double probabilityToRepair) {
        super(name, weapon, quantity);
        this.probabilityToRepair = probabilityToRepair;
    }

    public double getProbabilityToRepair() {
        return probabilityToRepair;
    }
}
