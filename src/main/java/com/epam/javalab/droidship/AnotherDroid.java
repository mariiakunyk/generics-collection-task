package com.epam.javalab.droidship;

public class AnotherDroid extends Droid {
    int anotherProbability;

    public AnotherDroid(String name, String weapon, int quantity, int anotherProbability) {
        super(name, weapon,quantity);
        this.anotherProbability = anotherProbability;
    }
}
