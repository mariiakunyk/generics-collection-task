package com.epam.javalab.priorityqueue;

import com.epam.javalab.droidship.AnotherDroid;
import com.epam.javalab.droidship.Droid;
import com.epam.javalab.droidship.RepairDroid;

import java.util.Comparator;

public class PriorityQueue<T> {
    private Node<T> head;
    private Comparator<T> comparator;

    private static class Node<T> {
        T item;
        Node<T> next;
        Node<T> previous;
    }

    public PriorityQueue(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    public boolean add(T item) {
        if (head == null) {
            head = createNode(item, null, null);
            return true;
        }

        Node<T> temp = head;

        while (true) {
            boolean goToNext = comparator.compare(temp.item, item) < 0;
            if (goToNext && temp.next == null) {
                temp.next = createNode(item, null, temp);

                return true;
            } else if (goToNext) {
                temp = temp.next;
            } else if (temp == head) {
                Node<T> newNode = createNode(item, temp, null);
                temp.previous = newNode;
                head = newNode;

                return true;
            } else {
                Node<T> newNode = createNode(item, temp, temp.previous);
                temp.previous.next = newNode;
                temp.previous = newNode;

                return true;
            }
        }
    }

    private Node<T> createNode(T item, Node<T> nextNode, Node<T> previousNode) {
        Node<T> temp = new Node<>();
        temp.item = item;
        temp.next = nextNode;
        temp.previous = previousNode;

        return temp;
    }

    public T poll() {

        if (head == null) {
            return null;
        } else if (head.next == null) {
            T element = head.item;
            head = null;
            return element;
        } else {
            T element = head.item;
            head = head.next;
            return element;
        }
    }

    public void clear() {
        if (head != null) {
            while (head.next != null) {
                Node<T> temp = head;
                head = head.next;
                temp = null;
            }
            head = null;
        }
    }

    public boolean remove(T element) {
        Node<T> temp = head;
        if (head.item == element) {
            head = head.next;
            return true;
        }


        while (temp.item != element) {
            temp = temp.next;
            if (temp == null) {
                return false;
            } else if (temp.item == element && temp.next == null) {
                temp.previous.next = null;
            } else if (temp.item == element) {
                temp.previous.next = temp.next;
                temp.next.previous = temp.previous;
            }
        }

        return true;
    }

    public int size() {
        Node<T> temp = head;
        int size = 0;
        while (temp != null) {
            temp = temp.next;
            size++;
        }
        return size;
    }

    public T peek() {
        if (head == null) {
            return null;
        } else if (head.next == null) {
            return head.item;
        } else {
            return head.item;
        }
    }

    public static void main(String[] args) {
        PriorityQueue<Droid> droidPriorityQueue =
                new PriorityQueue<>(new Droid.SortByQuantity());
        RepairDroid droidOne = new RepairDroid("semen", "ak47", 1, 3);
        droidPriorityQueue.add(new RepairDroid("oles", "colt", 5, 1));
        droidPriorityQueue.add(new RepairDroid("mykola", "colt", 4, 1));
        droidPriorityQueue.add(new RepairDroid("andrii", "colt", 2, 1));
        droidPriorityQueue.add(droidOne);
        droidPriorityQueue.add(new AnotherDroid("orest", "pistol", 3, 2));
        System.out.print("Queue size: ");
        System.out.println(droidPriorityQueue.size());
        droidPriorityQueue.remove(droidOne);
        System.out.print("Queue size after removing com.epam.javalab.droid: ");
        System.out.println(droidPriorityQueue.size());
        System.out.print("Name of first com.epam.javalab.droid in my queue with the least number of weapon: ");
        System.out.println(droidPriorityQueue.poll().getName());
        System.out.println(droidPriorityQueue.peek().getName());
    }
}


